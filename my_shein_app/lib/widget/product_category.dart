import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_shein_app/widget/product_detail.dart';

import '../network.dart';

class product_category_button extends StatelessWidget {
  const product_category_button({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchProductCategory(),
      builder: (
          BuildContext context,
          AsyncSnapshot snapshot
          ) {
        if(snapshot.hasData) {
          return ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: snapshot.data?.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  child: Column(
                    children: [
                      MaterialButton(
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute<void>(
                            builder: (BuildContext context) {
                              return Scaffold(
                                appBar: AppBar(
                                  elevation: 0,
                                  leading: IconButton(
                                    icon: Icon(Icons.arrow_back_ios, color: Colors.black,),
                                    onPressed: (){
                                      Navigator.pop(context);
                                    },
                                  ),
                                  titleTextStyle: TextStyle(color: Colors.black),
                                  backgroundColor: Colors.white,
                                  title: Text(snapshot.data[index].name),
                                ),
                                body: SingleChildScrollView(
                                 child: product_detail(),
                                ),
                              );
                            },
                          ));
                        },
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(snapshot.data[index].image),
                        ),
                      ),
                      Expanded(
                        child:SizedBox(
                        width: 100,
                        height: 60,
                        child: Title(
                            color: Colors.black,
                            child: Text(
                              snapshot.data[index].name,
                              textAlign: TextAlign.center,)
                        ),
                      ),),
                    ],
                  )
                );
              }
          );
        }
        else if(snapshot.hasError){
          return Container(
            child: Center(
              child: Text("Not found!!"),
            ),
          );
        }
        else {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }
}

class product_category_button2 extends StatelessWidget {
  const product_category_button2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchProductCategory2(),
      builder: (
          BuildContext context,
          AsyncSnapshot snapshot
          ) {
        if(snapshot.hasData) {
          return ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: snapshot.data?.length,
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  children: [
                    MaterialButton(
                      onPressed: () {
                        Navigator.push(context, MaterialPageRoute<void>(
                          builder: (BuildContext context) {
                            return Scaffold(
                              appBar: AppBar(
                                elevation: 0,
                                leading: IconButton(
                                  icon: Icon(Icons.arrow_back_ios, color: Colors.black,),
                                  onPressed: (){
                                    Navigator.pop(context);
                                  },
                                ),
                                titleTextStyle: TextStyle(color: Colors.black),
                                backgroundColor: Colors.white,
                                title: Text(snapshot.data[index].name),
                              ),
                              body: product_detail(),
                            );
                          },
                        ));
                      },
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                            snapshot.data[index].image
                        ),
                      ),
                    ),
                    Expanded(
                      child:SizedBox(
                        width: 100,
                        height: 60,
                        child: Title(
                            color: Colors.black,
                            child: Text(
                              snapshot.data[index].name,
                              textAlign: TextAlign.center,)
                        ),
                      ),),

                  ],
                );
              }
          );
        }
        else if(snapshot.hasError){
          return Container(
            child: Center(
              child: Text("Not found!!"),
            ),
          );
        }
        else {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }
}
