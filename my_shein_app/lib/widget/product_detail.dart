import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:my_shein_app/network.dart';

class product_detail extends StatelessWidget {
  const product_detail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: fetchProductDetail(),
      builder: (
          BuildContext context,
          AsyncSnapshot snapshot
          ) {
        if(snapshot.hasData) {
          return Column(
            children: [
              GridView.builder(
                physics: ScrollPhysics(),
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return MaterialButton(
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute<void>(
                        builder: (BuildContext context) {
                          return Scaffold(
                            appBar: AppBar(
                              elevation: 20,
                              leading: IconButton(
                                icon: Icon(Icons.arrow_back_ios, color: Colors.black,),
                                onPressed: (){
                                  Navigator.pop(context);
                                },
                              ),
                              titleTextStyle: TextStyle(color: Colors.black),
                              backgroundColor: Colors.white,
                              title: Text(snapshot.data[index].name),
                              actions: [
                                IconButton(onPressed: () {}, icon: Icon(Icons.shopping_bag_outlined), color: Colors.black,),
                                IconButton(onPressed: (){}, icon: Icon(Icons.menu_sharp), color: Colors.black,),
                              ],
                            ),
                            body: ListView(
                              children: [
                                Image.network(snapshot.data[index].image),
                                Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    Flexible(
                                      fit: FlexFit.loose,
                                      flex: 1,
                                      child: Title(color: Colors.black,
                                      child: Text(snapshot.data[index].name,
                                        style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w800,
                                          fontStyle: FontStyle.italic
                                        ),),
                                        
                                      )),
                                    Flexible(flex: 2,
                                      child: RaisedButton(
                                        onPressed: () {},
                                        color: Colors.redAccent,
                                        child: Center(
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(
                                                Icons.card_travel,
                                                color: Colors.white,
                                              ),
                                              SizedBox(
                                                width: 4.0,
                                              ),
                                              Text("Mua",
                                                style: TextStyle(color: Colors.white),
                                              ),
                                            ],
                                          ),
                                        ),),
                                    ),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(left: 12), child: Text(snapshot.data[index].price, style: TextStyle(
                                  fontSize: 20,
                                ),),)
                              ],
                            ),
                          );
                        },
                      ));
                    },
                    child: Column(
                      children: [
                        Expanded(
                            child: Image.network(
                              snapshot.data[index].image,
                              fit: BoxFit.fitHeight,
                            )
                        ),
                        Expanded(
                          child:Text(
                          snapshot.data[index].name,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                            fontWeight: FontWeight.w500,
                              fontStyle: FontStyle.italic,
                            ),),),
                      ],
                    ),
                  );
                },
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10
                ),
                padding: EdgeInsets.all(10),
                shrinkWrap: true,
              ),

            ],
          );
        }
        else if(snapshot.hasError){
          return Container(
            child: Center(
              child: Text("Not found!!"),
            ),
          );
        }
        else {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }
}
