import 'package:flutter/material.dart';

class Banner1st extends StatefulWidget {
  @override
  _BannerState createState() => _BannerState();
}

class _BannerState extends State<Banner1st> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      height: MediaQuery.of(context).size.height / 2.5,
      child: Card(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Image.asset(
                  "lib/assets/images/banner1.png"),
            ),
          ],
        ),
      ),
    );
  }
}

/*---------------------------------------------------------------*/
class Banner2st extends StatefulWidget {
  @override
  _BannerState2 createState() => _BannerState2();
}

class _BannerState2 extends State<Banner2st> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      height: MediaQuery.of(context).size.height / 3.5,
      child: Card(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Image.asset(
                  "lib/assets/images/banner2.jpg"),
            ),
          ],
        ),
      ),
    );
  }
}

/*---------------------------------------------------------------*/
class Banner3st extends StatefulWidget {
  @override
  _BannerState3 createState() => _BannerState3();
}

class _BannerState3 extends State<Banner3st> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      height: MediaQuery.of(context).size.height / 3.5,
      child: Card(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Image.asset(
                  "lib/assets/images/banner5.png"),
            ),
          ],
        ),
      ),
    );
  }
}

/*---------------------------------------------------------------*/
class Banner4st extends StatefulWidget {
  @override
  _BannerState4 createState() => _BannerState4();
}

class _BannerState4 extends State<Banner4st> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      height: MediaQuery.of(context).size.height / 3.5,
      child: Card(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Image.asset(
                  "lib/assets/images/banner6.png"),
            ),
          ],
        ),
      ),
    );
  }
}

/*---------------------------------------------------------------*/
class Banner5st extends StatefulWidget {
  @override
  _BannerState5 createState() => _BannerState5();
}

class _BannerState5 extends State<Banner5st> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      height: MediaQuery.of(context).size.height / 3.5,
      child: Card(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Image.asset(
                  "lib/assets/images/banner7.png"),
            ),
          ],
        ),
      ),
    );
  }
}


/*---------------------------------------------------------------*/
class Banner6st extends StatefulWidget {
  @override
  _BannerState6 createState() => _BannerState6();
}

class _BannerState6 extends State<Banner6st> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      height: MediaQuery.of(context).size.height / 3.5,
      child: Card(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Image.asset(
                  "lib/assets/images/banner8.png"),
            ),
          ],
        ),
      ),
    );
  }
}


