class product_detail {
  String? name;
  String? image;
  String? price;
  String? id;

  product_detail({this.name, this.image, this.price, this.id});

  product_detail.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    image = json['image'];
    price = json['price'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['image'] = this.image;
    data['price'] = this.price;
    data['id'] = this.id;
    return data;
  }
}
