class product_category {
  late String? name;
  late String? image;
  late String? id;

  product_category({required this.name, required this.image, required this.id});

  product_category.fromJson(Map<String, dynamic> json) {
    name = json['name'] ?? '';
    image = json['image'] ?? '';
    id = json['id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['image'] = this.image;
    data['id'] = this.id;
    return data;
  }
}

class product_category2 {
  String? name;
  String? image;
  String? id;

  product_category2({this.name, this.image, this.id});

  product_category2.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    image = json['image'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['image'] = this.image;
    data['id'] = this.id;
    return data;
  }
}
