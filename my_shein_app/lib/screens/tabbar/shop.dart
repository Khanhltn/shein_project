import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_shein_app/widget/deals_1.dart';
import 'package:my_shein_app/widget/product_category.dart';


class ShopSreen extends StatefulWidget{
  @override
    bodyShopSreen createState() => bodyShopSreen();
}

class bodyShopSreen extends State<ShopSreen> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: Column(
        children: [
          Banner1st(),
          SizedBox(
            child: Center(
              child: Text('Mua theo thể loại',
                style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 15,
              ),),
          ),),
          Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: product_category_button(),
          ),
          Padding(padding: EdgeInsets.all(5)),
          Flexible(
            flex: 2,
            fit: FlexFit.tight,
            child: product_category_button2(),
          ),
          // product_detail(),
        ],
        ),
      );
    // return ListView(
    //   children: [
    //     Banner1st(),
    //     SizedBox(child: Center(
    //       child: Text('Mua theo thể loại'),
    //     ),),
    //     Container(
    //       margin: EdgeInsets.all(20),
    //       child: SingleChildScrollView(
    //         scrollDirection: Axis.horizontal,
    //         child: Row(
    //           children: [
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {
    //                     Navigator.push(context, MaterialPageRoute<void>(
    //                       builder: (BuildContext context) {
    //                         return Scaffold(
    //                           appBar: AppBar(
    //                             titleTextStyle: TextStyle(color: Colors.black),
    //                             backgroundColor: Colors.white,
    //                             title: Text('Đầm'),
    //                           ),
    //                           body: Center(
    //                             child: FlatButton(
    //                               child: Text('Product detail'),
    //                               onPressed: () {
    //                                 Navigator.pop(context);
    //                               },
    //                             ),
    //                           ),
    //                         );
    //                       },
    //                     ));
    //                   },
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/product_10.webp'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Đầm')),
    //               ],
    //             ),
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/Product_2.png'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Áo thun')),
    //               ],
    //             ),
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/product_4.webp'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Đồ sơ mi')),
    //               ],
    //             ),
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/product_5.jpg'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Bottoms')),
    //               ],
    //             ),
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/product_6.webp'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Áo hai dây')),
    //               ],
    //             ),
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/product_7.webp'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Đồ bơi')),
    //               ],
    //             ),
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/product_8.webp'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Set đồ bộ')),
    //               ],
    //             ),
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/product_9.webp'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Jumpsuits & Bodysuits')),
    //               ],
    //             ),
    //           ],
    //         ),
    //       ),
    //     ),
    //     Container(
    //       child: SingleChildScrollView(
    //         scrollDirection: Axis.horizontal,
    //         child: Row(
    //           children: [
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/product_11.webp'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Plus size')),
    //               ],
    //             ),
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/product_12.webp'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Đồ lót & đồ mặt nhà')),
    //               ],
    //             ),
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/product_13.jpg'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Giày')),
    //               ],
    //             ),
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/product_14.jpg'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Denim')),
    //               ],
    //             ),
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/product_16.webp'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Túi')),
    //               ],
    //             ),
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/product_15.jpg'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Trang phục thể thao')),
    //               ],
    //             ),
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/Product_0.png'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Đẹp và cá nhân')),
    //               ],
    //             ),
    //             Column(
    //               children: [
    //                 RawMaterialButton(
    //                   onPressed: () {},
    //                   fillColor: Colors.white,
    //                   child: CircleAvatar(
    //                     backgroundImage: AssetImage('lib/assets/images/product_17.webp'),
    //                   ),
    //                   shape: CircleBorder(),
    //                 ),
    //                 Title(color: Colors.black, child: Text('Tất phụ nữ')),
    //               ],
    //             ),
    //
    //
    //           ],
    //         ),
    //       ),
    //     ),
    //   ],
    // );
  }

}