import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_shein_app/widget/product_detail.dart';
import '../../widget/deals_1.dart';

class PlusSizes extends StatefulWidget{
  @override
  bodySreen createState() => bodySreen();
}

class bodySreen extends State<PlusSizes> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        SizedBox(
          height: 20,
        ),
        SizedBox(
          height: 50,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              MaterialButton(onPressed: () {}, child: SizedBox(
                width: 100,
                child: Column(
                  children: [
                    Expanded(child:Icon(Icons.shopping_bag_outlined)),
                    Expanded(child: Text('Sale 10%'),),
                  ],
                ),
              ) ),
              MaterialButton(onPressed: () {}, child: SizedBox(
                width: 100,
                child: Column(
                  children: [
                    Expanded(child:Icon(Icons.fact_check_outlined)),
                    Expanded(child: Text('Đăng nhập'),),
                  ],
                ),
              ) ),
              MaterialButton(onPressed: () {}, child: SizedBox(
                width: 100,
                child: Column(
                  children: [
                    Expanded(child:Icon(Icons.point_of_sale)),
                    Expanded(child: Text('Flash Sale'),),
                  ],
                ),
              ) ),
              MaterialButton(onPressed: () {}, child: SizedBox(
                width: 100,
                child: Column(
                  children: [
                    Expanded(child:Icon(Icons.shopping_bag_outlined)),
                    Expanded(child: Text('Sale 10%'),),
                  ],
                ),
              ) ),
              MaterialButton(onPressed: () {}, child: SizedBox(
                width: 100,
                child: Column(
                  children: [
                    Expanded(child:Icon(Icons.shopping_bag_outlined)),
                    Expanded(child: Text('Sale 10%'),),
                  ],
                ),
              ) ),
            ],
          ),
        ),
        Banner6st(),
        Text('Sản phẩm nổi bật',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.bold
          ),
        ),
        product_detail(),
      ],
    );
  }

}