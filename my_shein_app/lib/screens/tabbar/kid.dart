import 'package:flutter/cupertino.dart';
import 'package:my_shein_app/widget/product_detail.dart';
import '../../widget/deals_1.dart';

class KidScreen extends StatefulWidget{
  @override
  bodySreen createState() => bodySreen();
}

class bodySreen extends State<KidScreen> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Banner3st(),
        Text('Sản phẩm nổi bật',
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.bold
          ),
        ),
        product_detail(),
      ],
    );
  }

}