import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../widget/deals_1.dart';

class Category extends StatefulWidget{
  @override
  bodySreen createState() => bodySreen();
}

class bodySreen extends State<Category> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.mail_outline, color: Colors.black,),
          onPressed: () {
            //onPressed
          },
        ),
        actions: [
          IconButton(onPressed: () {
            //onPressed
          },
              icon: Icon(Icons.camera_alt_outlined,color: Colors.black,)),
          IconButton(onPressed: () {
            //onPressed
          },
              icon: Icon(Icons.shopping_bag_outlined,color: Colors.black,)),
        ],
        backgroundColor: Colors.white,
        title: TextField(
          decoration: InputDecoration(
            hintText: 'type in journal name...',
            hintStyle: TextStyle(
              color: Colors.black,
              fontSize: 18,
              fontStyle: FontStyle.italic,
            ),
            border: InputBorder.none,
          ),
          style: TextStyle(
            color: Colors.black,
          ),
        ),
      ),
    );
  }

}