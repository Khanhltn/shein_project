import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_shein_app/screens/bottom/Loginscreen.dart';
import '../../models/user_model.dart';
import '../../ui/app.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  State<RegistrationScreen> createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {

  final _auth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();
  final firstNameEditingController = new TextEditingController();
  final secondNameEditingController = new TextEditingController();
  final emailEditingController = new TextEditingController();
  final passwordEditingController = new TextEditingController();
  final confirmPasswordEditingController = new TextEditingController();
  @override
  Widget build(BuildContext context) {

    //email field
    final emailField = TextFormField(
      autofocus: false,
      controller: emailEditingController,
      keyboardType: TextInputType.emailAddress,
      validator: (value) {
        if(value!.isEmpty){
          return ("please Enter Your Email");
        }
        // req expression for email validation
        if(!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]"
        ).hasMatch(value)){
          return ("Please Enter a valid email");
        }
        return null;
      },
      onSaved: (value) {
        emailEditingController.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: "Địa chỉ email",
      ),
    );
    //password field
    final passwordField = TextFormField(
      autofocus: false,
      controller: passwordEditingController,
      obscureText: true,
      validator: (value) {
        RegExp regex = new RegExp(r'^.{6,}$');
        if(value!.isEmpty){
          return("Password is required for login");
        }
        if(!regex.hasMatch(value))
        {
          return("Please Enter valid password(Min.6 password)");
        }
      },
      onSaved: (value) {
        passwordEditingController.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: "Mật mã",
      ),
    );
    //confirm passowrd field
    final confirmPasswordField = TextFormField(
      autofocus: false,
      controller: confirmPasswordEditingController,
      obscureText: true,

      validator: (value) {
        if(confirmPasswordEditingController.text.length != passwordEditingController.text.length){
          return "Password don't match";
        }
        return null;
      },
      onSaved: (value) {
        confirmPasswordEditingController.text = value!;
      },
      textInputAction: TextInputAction.done,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: "Nhập lại mật mã",
      ),
    );
    //Button
    final signUpButton = Material(
      elevation: 5,
      color: Colors.black,
      child: MaterialButton(
        padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        minWidth: MediaQuery.of(context).size.width,
        onPressed: (){
          signUp(emailEditingController.text, passwordEditingController.text);
        },
        child: Text(
          'ĐĂNG KÝ',
          textAlign: TextAlign.center,
          style: TextStyle
            (
              fontSize: 20,
              color: Colors.white,
              fontWeight: FontWeight.bold
          ),),
      ),
    );
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: Material(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(36.0),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    emailField,
                    SizedBox(height: 15,),
                    passwordField,
                    SizedBox(height: 15,),
                    confirmPasswordField,
                    SizedBox(height: 25,),
                    signUpButton,
                    SizedBox(height: 100,),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Hoặc tham gia"),
                        Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              IconButton(
                                icon: Image.network("https://freesvg.org/img/1534129544.png",),
                                onPressed: (){

                                },
                              ),
                              IconButton(
                                icon: Image.network("https://upload.wikimedia."
                                    "org/wikipedia/commons/1/16/Facebook-icon-1.png"),
                                onPressed: (){

                                },
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(child:Text("Bằng cách đăng "
                                "nhập vào tài khoản của bạn, "
                                "bạn đồng ý với Chính sách bảo mật "
                                "& Cookie và Điều khoản và Điều kiện của chúng tôi",
                              textAlign: TextAlign.center,),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void signUp(String email, String password) async
  {
    if(_formKey.currentState!.validate())
      {
        await _auth.createUserWithEmailAndPassword(email: email, password: password)
            .then((value) => {
          postDetailsToFirebase()

        }).catchError((e){
          Fluttertoast.showToast(msg: e!.message);
        });
      }
  }
  
  postDetailsToFirebase() async {
    
    //calling out firebase
    //calling our user model
    //sending these value
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    User? user = _auth.currentUser;
    
    UserModel userModel = UserModel();
    
    //writting all the value
    userModel.email = user!.email;
    userModel.uid = user.uid;
    
    await firebaseFirestore.collection("users").doc(user.uid).set(userModel.toMap());
    Fluttertoast.showToast(msg: "Account create successfully: ");
    
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginScreen()), result: (route) => false);
    

    
  }
}
