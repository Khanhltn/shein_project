

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../ui/app.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();


  final _auth = FirebaseAuth.instance;
  
  @override
  Widget build(BuildContext context) {

    //email field
    final emailField = TextFormField(
      autofocus: false,
      controller: emailController,
      keyboardType: TextInputType.emailAddress,

      validator: (value) {
        if(value!.isEmpty){
          return ("please Enter Your Email");
        }
        if(!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]"
        ).hasMatch(value)){
          return ("Please Enter a valid email");
        }
          return null;
      },
      onSaved: (value) {
        emailController.text = value!;
        },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        hintText: "Địa chỉ email",
      ),
    );

    //passsword field
    final passwordField = TextFormField(
      autofocus: false,
      obscureText: true,
      controller: passwordController,

      //validator: () {},
      validator: (value) {
        RegExp regex = new RegExp(r'^.{6,}$');
        if(value!.isEmpty){
          return("Password is required for login");
        }
        if(!regex.hasMatch(value))
          {
            return("Password need more than 5 character");
          }
      },

      onSaved: (value) {
        passwordController.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: "Mật mã",

      ),
    );

    //Button
    final loginButton = Material(
      elevation: 5,
      color: Colors.black,
      child: MaterialButton(
        padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        minWidth: MediaQuery.of(context).size.width,
        onPressed: (){
          signIn(emailController.text, passwordController.text);
        },
        child: Text(
          'Đăng nhập',
          textAlign: TextAlign.center,
        style: TextStyle
          (
            fontSize: 20,
            color: Colors.white,
            fontWeight: FontWeight.bold
        ),),
      ),
    );



    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    emailField,
                    SizedBox(height: 15,),
                    passwordField,
                    SizedBox(height: 45,),
                    loginButton,
                    SizedBox(height: 150,),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("- Hoặc tham gia -"),
                        Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              IconButton(
                                icon: Image.network("https://freesvg.org/img/1534129544.png",),
                                onPressed: (){

                                },
                              ),

                              IconButton(
                                icon: Image.network("https://upload.wikimedia."
                                    "org/wikipedia/commons/1/16/Facebook-icon-1.png"),
                                onPressed: (){

                                },
                              ),
                            ],
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(child:Text("Bằng cách đăng "
                                "nhập vào tài khoản của bạn, "
                                "bạn đồng ý với Chính sách bảo mật "
                                "& Cookie và Điều khoản và Điều kiện của chúng tôi",
                              textAlign: TextAlign.center,),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
  
  void signIn(String email, String password) async
  {
    if(_formKey.currentState!.validate())
      {
        await _auth
            .signInWithEmailAndPassword(email: email, password: password)
            .then((uid) => {
              Fluttertoast.showToast(msg: "Login Successful"),
              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => MyStatefulWidget()))
        }).catchError((e) {
          Fluttertoast.showToast(msg: e!.message);
        });
      }
  }
}
