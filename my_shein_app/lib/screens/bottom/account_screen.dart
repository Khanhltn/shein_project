import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_shein_app/screens/bottom/Loginscreen.dart';
import 'package:my_shein_app/screens/bottom/registrationscreen.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({Key? key}) : super(key: key);

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          titleTextStyle: TextStyle(
            color: Colors.black,
            fontSize: 30,
          ),
          title: Center(
            child: Text("SHEIN vn"),
          ),
          actions: [
            IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                  Icons.exit_to_app),
              color: Colors.black,
            )
          ],
          bottom: createTabBar(),
        ),
        body: TabBarView(
          children: [
            LoginScreen(),
            RegistrationScreen(),
          ],
        ),
      ),
    );
  }
  TabBar createTabBar()  {
    return TabBar(
      labelColor: Colors.black,
      tabs: [
        Tab (child: Text("ĐĂNG NHẬP")),
        Tab (child: Text("ĐĂNG KÝ")),
      ],
      isScrollable: true,
      indicatorColor: Colors.black,
    );
  }
}
