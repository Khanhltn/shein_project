import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_shein_app/screens/tabbar/house_animal.dart';
import 'package:my_shein_app/screens/tabbar/kid.dart';
import 'package:my_shein_app/screens/tabbar/males.dart';
import 'package:my_shein_app/screens/tabbar/plus_size.dart';
import 'package:my_shein_app/screens/tabbar/shop.dart';

class HomeScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return BodyScreen();
  }

}

class BodyScreen extends StatelessWidget
{
  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
        length: 5,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            titleTextStyle: TextStyle(
              color: Colors.black,
              fontSize: 30,
            ),
            title: Center(
              child: Text("SHEIN vn"),
            ),
            leading: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: IconButton(
                    icon: Icon(
                        Icons.mail,
                        color: Colors.black
                    ),
                    onPressed: () {

                    },
                  ),),

                Flexible(
                  child: IconButton(
                      onPressed: (){

                      },
                      icon: Icon(
                        Icons.favorite,
                        color: Colors.black,
                      )
                  ),)
              ],
            ),
            actions: [
              IconButton(
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute<void>(
                      builder: (BuildContext context) {
                        return Scaffold(
                          appBar: AppBar(
                            leading: IconButton(
                              icon: Icon(Icons.arrow_back_ios, color: Colors.black,),
                              onPressed: (){
                                Navigator.pop(context);
                              },
                            ),
                            titleTextStyle: TextStyle(color: Colors.black),
                            backgroundColor: Colors.white,
                            title: TextField(
                              decoration: InputDecoration(
                                hintText: 'type in product...',
                                hintStyle: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18,
                                  fontStyle: FontStyle.italic,
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        );
                      },
                    ));
                  },
                  icon: Icon(
                    Icons.search,
                    color: Colors.black,
                  )
              ),
              IconButton(
                onPressed: (){} ,
                icon: Icon(
                  Icons.shop,
                  color: Colors.black,
                ),
              ),
            ],
            bottom: createTabBar(),
          ),
          body: TabBarView(
            children: [
              ShopSreen(),
              PlusSizes(),
              KidScreen(),
              MaleScreen(),
              HouseAnimal(),
            ],
          ),
        )
    );
  }

  TabBar createTabBar()  {
    return TabBar(
        labelColor: Colors.black,
        tabs: [
          Tab (child: Text("SHOP")),
          Tab (child: Text("PLUS SIZES")),
          Tab (child: Text("TRẺ EM")),
          Tab (child: Text("NAM")),
          Tab (child: Text("NHÀ + VẬT NUÔI")),
        ],
        isScrollable: true,
        indicatorColor: Colors.black,
    );
  }

}