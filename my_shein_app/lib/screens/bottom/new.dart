import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_shein_app/widget/product_detail.dart';

import '../../widget/deals_1.dart';

class News extends StatefulWidget{
  @override
  bodySreen createState() => bodySreen();
}

class bodySreen extends State<News> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text('New Product for you'),
      ),
      body: ListView(
        children: [
          Banner2st(),
          SizedBox(
            child: Text('Các sản phẩm mới',textAlign: TextAlign.center,),
          ),
          product_detail(),
        ],
      ),
    );
  }

}