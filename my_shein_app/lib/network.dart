import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:my_shein_app/models/product_detail.dart';

import 'models/product_category.dart';
import 'package:http/http.dart' as http;

List<product_category> parseProductCategory(String responseBody){
  var list = json.decode(responseBody) as List<dynamic>;
  List<product_category>? productcategory = list.map((model) => product_category.fromJson(model)).toList();
  return productcategory;
}

Future<List<product_category>> fetchProductCategory() async{
  String url = 'https://6272406fc455a64564be3960.mockapi.io/api/products';
  Uri uri = Uri.parse(url);
  final response = await http.get(uri);
  if(response.statusCode == 200)
    {
      return compute(parseProductCategory, response.body);
    } else {
    throw Exception('Request API Error');
  }
}

List<product_category2> parseProductCategory2(String responseBody2){
  var list = json.decode(responseBody2) as List<dynamic>;
  List<product_category2>? productcategory2 = list.map((model) => product_category2.fromJson(model)).toList();
  return productcategory2;
}

Future<List<product_category2>> fetchProductCategory2() async{
  String url = 'https://shein-products.mocklab.io/list_products';
  Uri uri = Uri.parse(url);
  final response = await http.get(uri);
  if(response.statusCode == 200)
  {
    return compute(parseProductCategory2, response.body);
  } else {
    throw Exception('Request API Error');
  }
}

List<product_detail> parseProductDetail(String responseBody2){
  var list = json.decode(responseBody2) as List<dynamic>;
  List<product_detail>? productdetail = list.map((model) => product_detail.fromJson(model)).toList();
  return productdetail;
}

Future<List<product_detail>> fetchProductDetail() async{
  String url = 'https://shein-products.mocklab.io/list_product_detail';
  Uri uri = Uri.parse(url);
  final response = await http.get(uri);
  if(response.statusCode == 200)
  {
    return compute(parseProductDetail, response.body);
  } else {
    throw Exception('Request API Error');
  }
}