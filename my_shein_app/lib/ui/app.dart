import 'package:flutter/material.dart';
import 'package:my_shein_app/screens/bottom/account_screen.dart';
import 'package:my_shein_app/screens/bottom/category.dart';
import 'package:my_shein_app/screens/bottom/profile_screen.dart';
import '../screens/bottom/homescreen.dart';
import '../screens/bottom/new.dart';

class myApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "SHEIN",
      debugShowCheckedModeBanner: false,
      home: AccountScreen(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  final screens = [
    HomeScreen(),
    Category(),
    News(),
    ProfileScreen(),
  ];


  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        showUnselectedLabels: false,

        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Mua sắm',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Danh mục',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.star),
            label: 'Mới',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.login),
            label: 'Tôi',
          ),

        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.black,
        onTap: _onItemTapped,
      ),
    );
  }
}